\documentclass{scrartcl}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{array}
\usepackage{ebproof}
\usepackage{graphicx}
\usepackage{minted}
\usepackage{pdflscape}
\usepackage{stmaryrd}
\usepackage{syntax}

\renewcommand{\arraystretch}{2.5}
\newtheorem{mydef}{Definition}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}

\newcommand{\IF}{\textbf{if }}
\newcommand{\THEN}{\textbf{ then }}
\newcommand{\ELSE}{\textbf{ else }}
\newcommand{\FN}{\textbf{fn }}
\newcommand{\TRUE}{\textbf{true}}
\newcommand{\FALSE}{\textbf{false}}
\newcommand{\UNCURRY}{\textbf{uncurry}}
\newcommand{\ZEROU}{\textbf{zerou}}
\newcommand{\SUCC}{\textbf{succ}}
\newcommand{\PRED}{\textbf{pred}}

\title{Uniqueness Types in PCF}
\subtitle{Semantics and Domain Theory Assignment}
\author{Erin van der Veen}

\begin{document}

\maketitle

\section{Uniqueness Types}
Uniqueness types are a method for Clean (and Mercury/Idris) to deal with side-effects.
Moreover, it allows the compiler to perform optimizations in a way similar to Linear Types.
Consider the following Clean program:
\begin{minted}{Clean}
get2c :: File -> (Char, Char)
get2c f
# a = freadc f
# b = freadc f
= (a, b)
\end{minted}
Let it be clear that we expect \mintinline{Clean}|freadc f| to return different characters at these calls.
However, given that Clean is a pure functional programming language, and that \mintinline{Clean}|f| is not modified between the two calls, it should be such that \mintinline{Clean}|freadc f| returns the same character twice.
To solve this we could write:
\begin{minted}{Clean}
get2c :: File -> (Char, Char)
get2c f
# (a, f`) = freadc f
# (b, f``) = freadc f`
= (a, b)
\end{minted}
Such that the file passed to \mintinline{Clean}|freadc| is conceptually different each time.
The compiler enforces this through the use of uniqueness:
\begin{minted}{Clean}
get2c :: *File -> (Char, Char)
get2c f
# (a, f) = freadc f
# (b, _) = freadc f
= (a, b)
\end{minted}

For this assignment, I will extend PCF with uniqueness.

\section{Syntax}
We must first define the syntax of the new PCFu (PCF with uniqueness) language.

\subsection{Typing}
	There are multiple ways to approach this problem.
	In the first, we simply define $\tau$ as:
	\begin{align*}
		\tau ::= nat \mid bool \mid \tau \rightarrow \tau \mid nat^* \mid bool^* \mid \tau \rightarrow^* \tau
	\end{align*}

	Another option would be to define the types in the following way:
	\begin{align*}
		\gamma &::= nat \mid bool \mid \tau \rightarrow \tau\\
		\mu &::= u \mid n\\
		\tau &::= (\gamma, \mu)
	\end{align*}
	where $u$ means unique and $n$ means non-unique.

	We will use the second approach.
	Additionally we will define $\gamma^* := (\gamma, u)$ and $\gamma := (\gamma, n)$.
	This notation is borrowed from the Clean syntax.
\subsection{Expressions}
	\begin{align*}
		M ::= 0 &\mid \textbf{succ}(M) \mid \PRED(M) \mid \textbf{zero} \mid \textbf{true} \mid \textbf{false} \mid \textbf{if } M \textbf{ then } M \textbf{ else } M\\
		&\mid x \mid \textbf{fn } x : \tau.M \mid M M \mid \textbf{fix}(M)
	\end{align*}

\section{Typing}
We do not need to redefine the $fv : PCF \rightarrow 2^\mathbb{V}$ function.
We do need to create another environment.
\begin{mydef}
	$\Theta$ is a \emph{uniqueness environment}, a partial function from the set of (unique) variables to $\mu$ value.
	This environment defines if a variable is unique.
	\[
		\Theta : \mathbb{V} \rightharpoondown \{u, n\}
	\]
\end{mydef}

We can then define the typing relations on the PCF terms.

\begin{center}
\begin{prooftree}
	\hypo{\Gamma, \Theta \vdash 0 : nat}
\end{prooftree}
\\[2em]
\begin{prooftree}
	\hypo{\Gamma, \Theta \vdash 0 : nat^*}
\end{prooftree}
\\[2em]
\begin{prooftree}
	\hypo{\Gamma, \Theta \vdash M : nat}
	\infer1[where $y \in \{u, n\}$]{\Gamma, \Theta \vdash \textbf{succ}(M) : (nat, y)}
\end{prooftree}
\\[2em]
\begin{prooftree}
	\hypo{\Gamma, \Theta \vdash M : nat*}
	\infer1[where $y \in \{u, n\}$]{\Gamma, \Theta \vdash \textbf{succ}(M) : (nat, y)}
\end{prooftree}
\\[2em]
\begin{prooftree}
	\hypo{\Gamma, \Theta \vdash M : nat}
	\infer1[where $y \in \{u, n\}$]{\Gamma, \Theta \vdash \PRED(M) : (nat, y)}
\end{prooftree}
\\[2em]
\begin{prooftree}
	\hypo{\Gamma, \Theta \vdash M : nat*}
	\infer1[where $y \in \{u, n\}$]{\Gamma, \Theta \vdash \PRED(M) : (nat, y)}
\end{prooftree}
\\[2em]
\begin{prooftree}
	\hypo{\Gamma, \Theta \vdash M : nat}
	\infer1[where $y \in \{u, n\}$]{\Gamma, \Theta \vdash \textbf{zero}(M) : (bool, y)}
\end{prooftree}
\\[2em]
\begin{prooftree}
	\hypo{\Gamma, \Theta \vdash M : nat*}
	\infer1[where $y \in \{u, n\}$]{\Gamma, \Theta \vdash \textbf{zero}(M) : (bool, y)}
\end{prooftree}
\\[2em]
\begin{prooftree}
	\hypo{\Gamma, \Theta \vdash b : bool}
\end{prooftree}\ (b = true, false)
\\[2em]
\begin{prooftree}
	\hypo{\Gamma, \Theta \vdash b : bool^*}
\end{prooftree}\ (b = true, false)
\\[2em]
\begin{prooftree}
	\hypo{\Gamma, \Theta \vdash M_1 : (bool, y)}
	\hypo{\Gamma, \Theta \vdash M_2 : \tau}
	\hypo{\Gamma, \Theta \vdash M_3 : \tau}
	\infer3[where $y \in \{u, n\}$]{\Gamma, \Theta \vdash \textbf{if } M_1 \textbf{ then } M_2 \textbf{ else } M_3 : \tau}
\end{prooftree}
\\[2em]
\begin{prooftree}
	\hypo{\Gamma, \Theta \vdash x : (\gamma, u)}
\end{prooftree} if  $x$  is used once in the context and $\gamma \in dom(\Gamma) \land \Gamma(x) = \gamma \land \Theta(x) = u$
\\[2em]
\begin{prooftree}
	\hypo{\Gamma, \Theta \vdash x : (\gamma, n)}
\end{prooftree} if $\gamma \in dom(\Gamma) \land \Gamma(x) = \gamma \land \Theta(x) = n$
\\[2em]
\begin{prooftree}
	\hypo{\Gamma[x \mapsto \gamma], \Theta[x \mapsto \mu] \vdash M : \tau}
	\infer1[where $x \not\in dom(\Gamma)$ and $y = u$ if $M$ has unique variables]{\Gamma, \Theta \vdash \textbf{fn } x : (\gamma, \mu).M : (\gamma, \mu) \rightarrow^y \tau}
\end{prooftree}
\\[2em]
\begin{prooftree}
	\hypo{\Gamma, \Theta \vdash M : (\tau \rightarrow \tau', y)}
	\hypo{\Gamma, \Theta \vdash e : \tau}
	\infer2{\Gamma, \Theta \vdash Me : \tau'}
\end{prooftree}
\\[2em]
\begin{prooftree}
	\hypo{\Gamma, \Theta \vdash M : (\tau \rightarrow \tau, y)}
	\infer1[where $y \in \{u, n\}$]{\Gamma, \Theta \vdash \textbf{fix}(M) : \tau}
\end{prooftree}
\end{center}
In our new typing system, Proposition 5.3.1 of the lecture notes no longer holds.
\paragraph{Counterexample:} Let $M = 0$, then $M : nat$ but also $M : nat^*$.

\subsection{Examples}
In this section we will consider two examples.
Example~\ref{fig:ex1} shows an example where uniqueness is properly used.
Example~\ref{fig:ex2} shows an example where the uniqueness constraints are violated.
\begin{landscape}
\begin{figure}[t]
	\scalebox{0.7}{
		\begin{prooftree}
			\hypo{[b \mapsto bool, x \mapsto nat], [b \mapsto n, x \mapsto u] \vdash b : bool}
			\hypo{[b \mapsto bool, x \mapsto nat], [b \mapsto n, x \mapsto u] \vdash x : nat*}
			\infer1{[b \mapsto bool, x \mapsto nat], [b \mapsto n, x \mapsto u] \vdash \PRED(x) : nat}
			\hypo{[b \mapsto bool, x \mapsto nat], [b \mapsto n, x \mapsto u] \vdash 0 : nat}
			\infer3{[b \mapsto bool, x \mapsto nat], [b \mapsto n, x \mapsto u] \vdash \textbf{if } b \textbf{ then } \PRED(x) \textbf{ else } 0 : nat}
			\infer1{[b \mapsto bool], [b \mapsto n] \vdash \textbf{fn } x : nat^*.\textbf{if } b \textbf{ then } \PRED(x) \textbf{ else } 0 : nat^* \rightarrow nat}
			\infer1{\vdash \textbf{fn } b : bool.\textbf{fn } x : nat^*.\textbf{if } b \textbf{ then } \PRED(x) \textbf{ else } 0 : bool \rightarrow nat^* \rightarrow nat}
			\hypo{\vdash true : bool}
			\infer2{\vdash (\textbf{fn } b : bool.\textbf{fn } x : nat^*.\textbf{if } b \textbf{ then } \PRED(x) \textbf{ else } 0)true : nat^* \rightarrow nat}
			\hypo{\vdash 0 : nat^*}
			\infer1{\vdash succ(0) : nat^*}
			\infer2{ \vdash ((\textbf{fn } b : bool.\textbf{fn } x : nat^*.\textbf{if } b \textbf{ then } \PRED(x) \textbf{ else } 0)true)succ(0) : nat}
		\end{prooftree}
	}
	\caption{Example of a type tree for a well-typed expression with uniqueness}
	\label{fig:ex1}
\end{figure}
\begin{figure}[t]
	\scalebox{0.7}{
		\begin{prooftree}
			\hypo{[b \mapsto bool, x \mapsto nat], [b \mapsto n, x \mapsto u] \vdash b : bool}
			\hypo{[b \mapsto bool, x \mapsto nat], [b \mapsto n, x \mapsto u] \vdash x : nat*}
			\infer1{[b \mapsto bool, x \mapsto nat], [b \mapsto n, x \mapsto u] \vdash \PRED(x) : nat}
			\hypo{\text{Could not be derived. x is of type }nat^*}
			\infer1{[b \mapsto bool, x \mapsto nat], [b \mapsto n, x \mapsto u] \vdash succ(x) : nat}
			\infer3{[b \mapsto bool, x \mapsto nat], [b \mapsto n, x \mapsto u] \vdash \textbf{if } b \textbf{ then } \PRED(x) \textbf{ else } succ(x) : nat}
			\infer1{[b \mapsto bool], [b \mapsto n] \vdash \textbf{fn } x : nat^*.\textbf{if } b \textbf{ then } \PRED(x) \textbf{ else } succ(x) : nat^* \rightarrow nat}
			\infer1{\vdash \textbf{fn } b : bool.\textbf{fn } x : nat^*.\textbf{if } b \textbf{ then } \PRED(x) \textbf{ else } succ(x) : bool \rightarrow nat^* \rightarrow nat}
			\hypo{\vdash true : bool}
			\infer2{\vdash (\textbf{fn } b : bool.\textbf{fn } x : nat^*.\textbf{if } b \textbf{ then } \PRED(x) \textbf{ else } succ(x))true : nat^* \rightarrow nat}
			\hypo{\vdash 0 : nat^*}
			\infer1{\vdash succ(0) : nat^*}
			\infer2{ \vdash ((\textbf{fn } b : bool.\textbf{fn } x : nat^*.\textbf{if } b \textbf{ then } \PRED(x) \textbf{ else } succ(x))true)succ(0) : nat}
		\end{prooftree}
	}
	\caption{Example of a type tree for a non well-typed expression with uniqueness}
	\label{fig:ex2}
\end{figure}
\end{landscape}

Adding uniqueness to PCF does not require further modifications, as uniqueness is solely a typing attribute.
However, there are some things that we would like to do with unique values, that are simply not possible as is.
Consider, for example, the following expression that decrements a value with 1 unless that value is 0:
\[
	\FN x : nat^*.\IF zero(x) \THEN 0 \ELSE \PRED(x)
\]
Not only is this function not well typed, but it is impossible to create another function with the same semantics as this one.

\subsection{Tuples}
In the remainder of this and the following two sections, we will extend PCF with the functionality needed to create this function.
We will achieve this by adding another type to the language (tuples) and subsequently create functions that make use of this new datatype.
\begin{align*}
	\gamma &::= nat \mid bool \mid \tau \rightarrow \tau \mid (\tau, \tau)\\
	\mu &::= u \mid n\\
	\tau &::= (\gamma, \mu)
\end{align*}

With this tuple, we will want to be able to write some function like this:
\[
	\FN v : nat^*.\UNCURRY((\FN b : bool. \FN x : nat^*.\IF b \THEN 0 \ELSE \PRED(x)), \ZEROU(v))
\]
Where:
\begin{align*}
	\UNCURRY &: (\tau \rightarrow \tau' \rightarrow \tau'') \rightarrow (\tau, \tau') \rightarrow \tau''\\
	\ZEROU &: nat^* \rightarrow (bool, nat^*)^*
\end{align*}

The type trees of these functions are:
\[
\begin{prooftree}
    \hypo{\Gamma, \Theta \vdash M' : (\tau, \tau')}
    \hypo{\Gamma, \Theta \vdash M : \tau \rightarrow \tau' \rightarrow \tau''}
    \infer2{\Gamma, \Theta \vdash \UNCURRY(M, M') : \tau''}
\end{prooftree}
\]
\[
\begin{prooftree}
    \hypo{\Gamma, \Theta \vdash M : nat^*}
    \infer1{\Gamma, \Theta \vdash \ZEROU(M) : (bool, nat^*)^*}
\end{prooftree}
\]

\section{Operational Semantics}
We will only look at the additional functions that we introduced.
\[
\begin{prooftree}
    \hypo{M \Downarrow_{nat^*} 0}
    \infer1{\ZEROU(M) \Downarrow_{(bool, nat^*)^*} (\TRUE, 0)}
\end{prooftree}
\]
\[
\begin{prooftree}
    \hypo{M \Downarrow_{nat^*} succ(V)}
    \infer1{\ZEROU(M) \Downarrow_{(bool, nat^*)^*} (\FALSE, succ(V))}
\end{prooftree}
\]
\[
\begin{prooftree}
    \hypo{M \Downarrow_{\tau \rightarrow \tau' \rightarrow \tau''} \FN x : \tau. \FN y : \tau'. M'}
    \hypo{N \Downarrow_{(\tau, \tau')} (V, V')}
    \hypo{M[V/x, V'/y] \Downarrow_{\tau''} V''}
    \infer3{\UNCURRY(M, N) \Downarrow_{\tau''} V''}
\end{prooftree}
\]

We can now derive the function that I created above.
\begin{landscape}
\begin{figure}
    \begin{prooftree}
        \hypo{1}
        \hypo{1}
        \hypo{\SUCC(0) \Downarrow_{nat^*} \SUCC(0)}
        \infer1{\ZEROU(\SUCC(0)) \Downarrow_{(bool, nat^*)} (\FALSE, \SUCC(0))}
        \hypo{\FALSE \Downarrow_{bool} \FALSE}
        \hypo{\SUCC(0) \Downarrow_{nat^*} \SUCC(0)}
        \infer1{\PRED(\SUCC(0)) \Downarrow_{nat^*} 0}
        \infer2{\IF \FALSE \THEN 0 \ELSE \PRED(\SUCC(0)) \Downarrow_{nat^*} 0}
        \infer3{\UNCURRY((\FN b : bool . \FN x : nat^*.\IF b \THEN 0 \ELSE \PRED(x)), \ZEROU(\SUCC(0))) \Downarrow_{nat^*} 0}
        \infer2{\vdash (\FN v : nat^*.\UNCURRY((\FN b : bool . \FN x : nat^*.\IF b \THEN 0 \ELSE \PRED(x)), \ZEROU(v))\SUCC(0) \Downarrow_{nat^*} 0}
    \end{prooftree}
    \caption{The Derivation of the clamp function with uniqueness}
    \label{fig:ex3}
\end{figure}

\begin{enumerate}
    \item These derivation trees are removed for brevity
\end{enumerate}
\end{landscape}

\section{Denotational Semantics}
Like with the operational semantics, we will only define the rules that are non-trivial.
In this case, that means that we will only define the rules regarding the new functions.
We saw earlier that Proposition 5.3.1 no longer holds.
This is of influence on the denotational semantics, since the lecture notes only describe creating denotational semantics for terms for which this proposition does hold.
This is not a big problem.
Recall that uniqueness typing has no influence on the semantics of PCF, rather, it simply restricts the number of terms that is well-typed.
We can therefore assume that the semantics of any program will be identical to the same program where all uniqueness is removed.
\begin{lemma}
    \[
        \llbracket M \rrbracket(\rho) = \llbracket N \rrbracket(\rho)
    \]
    Where $N = M$ with all uniqueness removed. Note that this lemma only holds if the uniqueness restriction of $zerou$ is removed.
\end{lemma}
Note that the opposite is not true, since the term might not be well-typed if we add uniqueness constraints.

\subsection{Tuples Domain}
We need to create a Tuples domain before we can define the functions that use this domain.
Fortunately, creating this domain is trivial.
\begin{mydef}
    The tuple $\llbracket(\tau, \tau')\rrbracket$ is the trivial flat domain of tuples.
\end{mydef}

\subsection{Denotation of Terms}
We can now define the denotational semantics of the \textbf{zerou} and \textbf{uncurry} functions:
\[
    \llbracket \Gamma \vdash \ZEROU(M) \rrbracket (\rho) =
        \begin{cases}
            (true, \llbracket \Gamma \vdash M \rrbracket (\rho)) & \text{if } \llbracket \Gamma \vdash M \rrbracket (\rho) = 0\\
            (false, \llbracket \Gamma \vdash M \rrbracket (\rho)) & \text{if } \llbracket \Gamma \vdash M \rrbracket (\rho) > 0\\
            \bot & \text{if } \llbracket \Gamma \vdash M \rrbracket (\rho) = \bot\\
        \end{cases}
\]
\begin{align*}
    \llbracket \Gamma \vdash \UNCURRY(M,N) \rrbracket (\rho) =
            (\llbracket \Gamma \vdash M \rrbracket (\rho))(\llbracket \Gamma \vdash N_1 \rrbracket (\rho))(\llbracket \Gamma \vdash N_2 \rrbracket (\rho))\\\text{where } (N_1, N_2) = \llbracket \Gamma \vdash N \rrbracket (\rho)
\end{align*}

\section{Generalization}
The method described above (to generate a function with uniqueness types from one that cannot support them) can be generalized.
Let us look at the general pattern.
We have a (non-unique) function of the following form:
\[
    (\FN x.g(f(x), M, \dots f'(x), M'))V
\]
Where x is used at least twice.
We saw above that, if x is used twice, we can transform the function to the following:
\[
    \UNCURRY((\FN v.\FN x.g(v, M, \dots f'(x), M')), fu(V))
\]
Where $fu$ is a function of the following operational semantics:
\[
\begin{prooftree}
    \hypo{f(M) \Downarrow_{\tau} M'}
    \infer1{fu(M) \Downarrow_{(\tau', \tau)} (M', M)}
\end{prooftree}
\]
This method can be used recursively for functions that use a unique value more than once:
\[
    (\FN x.g(f_0(x), f_1(x), \dots f_n(x)))V
\]
would be:
\[
    \UNCURRY(\FN x_0. \dots \UNCURRY(\FN x_{n - 1}.\FN x. g(x_0, x_1, \dots f_n(x)), f_{n-1}(x_{n-2}) \dots f_0(V))
\]

To complete this transformation, let us define the denotational semantics of $fu$
\[
    \llbracket \Gamma \vdash fu(M) \rrbracket(\rho) = (\llbracket \Gamma \vdash f(M) \rrbracket(\rho), \llbracket \Gamma \vdash M \rrbracket(\rho))
\]
\end{document}
