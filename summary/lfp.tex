\section{Least Fixed Points}
\subsection{Domains}
\begin{mydef}[Partial Order]
	A binary relation $\LT$ on a set $D$ with:
	\begin{description}
		\item[Reflexive] $\forall d \in D. d \LT d$
		\item[Transitive] $\forall d, d', d'' \in D. d \LT d' \LT d'' \Rightarrow d \LT d''$
		\item[Anti-Symmentric] $\forall d, d' \in D. d \LT d' \LT d \Rightarrow d = d'$
	\end{description}
\end{mydef}

\begin{mydef}[Partially Ordered Sets (poset)]
	A tuple $(D, \LT)$ of a set $D$ and a partial order $\LT$.
\end{mydef}

\begin{mydef}[Least Element]
	Given a poset $D$, $S \subseteq D$, and $d \in S$.
	$d$ is then the \emph{Least Element} of $S$ if:
	\[
		\forall x \in S. d \LT x
	\]
\end{mydef}

\begin{lemma}
	The subset $S$ of a poset $D$ has at most one least element.
\end{lemma}
\begin{proof}
	Suppose $e$ and $d$ are lub of $(d_i)_{i \in \mathbb{N}}$ then:
	$d \LT e$ and $e \LT d$, thus $d = e$.
\end{proof}

\begin{lemma}
	The least element of a subset of a poset need not exist.
\end{lemma}
Consider for example $\mathbb{Z}$.

\begin{mydef}[$\bot_D$]
	The least element of a poset $D$. Note:
	\[
		\forall d \in D. \bot_D \LT d
	\]
\end{mydef}

\begin{mydef}[Chain]
	A countable increasing \emph{chain} in a poset D is a sequence of elements of $D$ satisfying:
	\[
		d_0 \LT d_1 \LT d_2 \LT \dots
	\]
\end{mydef}

\begin{mydef}[Upper Bound]
	The upper bound of a chain is the element $d \in D$ for which
	\[
		\forall n \in \mathbb{N}. d_n \LT d
	\]
	Note: the upper bound need not be part of the chain.
\end{mydef}

\begin{mydef}[Least Upper Bound (lub)]
	The least upper bound will be written as:
	\[
		\bigsqcup_{n \geq 0} d_n
	\]
	Note: we have the following:
	\[
		\forall m \in \mathbb{N}. d_m \LT \bigsqcup_{n \geq 0} d_n
	\]
	\[
		\text{For any } d \in D, \text{ if } \forall m \in \mathbb{N}. d_m \LT d, \text{ then } \bigsqcup_{n \geq 0} d_n \LT d.
	\]
\end{mydef}
NOTE: Not every chain has a lub. Consider for example $\mathbb{N}$.

\begin{mydef}[Chain Complete Poset (cpo)]
	A poset $(D, \LT)$ in which all chains $d_0 \LT d_1 \LT \dots$ have lubs.
\begin{description}
	\item[lub1] \[
			\forall m \geq 0.d_m \LT \LUB
		\]
	\item[lub2] \[
			\forall d \in D. (\forall m \geq 0. d_m \LT d) \Rightarrow \LUB \LT d.
		\]
\end{description}
\end{mydef}

\begin{mydef}[Domain]
	A cpo that possesses a least element, $\bot$.
	\[
		\forall d \in D. \bot \LT d
	\]
\end{mydef}

\subsection{Domain of Partial Functions}
Given the set of partial functions: $X \rightharpoondown Y$ we can define a domain on this set.

\begin{mydef}[Domain of Partial Functions]
	The domain of the partial functions:
	\begin{description}
		\item[Underlying set] all partial functions $f$ with $dom(f) \subseteq X$
		\item[Partial order] $f \LT g \Leftrightarrow dom(f) \subseteq dom(g)$ and $\forall x \in dom(f) . f(x) = g(x)$
		\item[Lub of Chain] $f_0 \LT f_1 \LT \dots$ is the partial function f with $dom(f) = \bigcup_{n \geq 0} dom(f_n)$ and 
			\[
				f(x) =
				\begin{cases}
					f_n(x) &\text{if } x \in dom(f_n), \text{ for some } n\\
					\text{undefined} & \text{undefined}
				\end{cases}
			\]
		\item[$\bot$] is the totally undefined partial function
	\end{description}
\end{mydef}
The partial order can be seen as an information order where $f \LT g$ means that g conveys more information than $f$.

\begin{mydef}[Monotonicity]
	A function $f : D \rightarrow E$ between posets is \emph{monotone} iff
	\[
		\forall d, d' \in D. d \LT d' \Rightarrow f(d) \LT f(d')
	\]
\end{mydef}

\begin{mydef}[Continuity]
	If $D$ and $E$ are cpo's, the function $f$ is continuous iff it is monotone and preserves lubs of chains. For all chains: $d_0 \LT d_1 \dots$ in $D$, it holds that:
	\[
		f(\LUB) = \LU f(d_n) \text{ in } E
	\]
\end{mydef}

\begin{mydef}[Strictness]
	A function $f$ is \emph{strict} iff
	\[
		f(\bot) = \bot
	\]
\end{mydef}

\subsection{Tarski's fixed point theorem}
Recall the notion of fixed points.
\begin{mydef}[Pre-fixed point]
	Let $D$ be a poset and $f : D \rightarrow D$ a function.
	An element $d \in D$ is a \emph{pre-fixed point} of $f$ if it satisfies $f(d) \LT d$
	The least pre-fixed point of $f$, if it exists, will be written as:
	\[
		fix(f)
	\]
\end{mydef}

\begin{mydef}[$fix(f)$]
	Properties of fix:
	\begin{description}
		\item[lfp1] $f(fix(f)) \LT fix(f)$
		\item[lfp2] $\forall d \in D. f(x) \LT d \Rightarrow fix(f) \LT d$
	\end{description}
\end{mydef}

\begin{proposition}
	Suppose $D$ is a poset and $f : D \rightarrow D$ is a function possessing a least pre-fixed point. Provided $f$ is monotone, $fix(f)$ is in particular a fixed point for $f$ (and hence is the least element of the set of fixed points for $f$).
\end{proposition}

\begin{mydef}[Tarski's Fixed Point Theorem]
	Let $f : D \rightarrow D$ be a continuous function on a domain D.
	Then:
	\begin{itemize}
		\item $f$ possesses a least pre-fixed point, given by
			\[
				fix(f) = \LU f^n(\bot)
			\]
		\item $fix(f)$ is a fixed point of $f$, $f(fix(f)) = fix(f)$ and hence is the least fixed point of $f$.
	\end{itemize}
\end{mydef}

\subsection{Additionally}
\begin{mydef}[Eventually Constant]
	A chain $(d_i)_{i \in \mathbb{N}}$ is eventually constant in case there is a $N \in \mathbb{N}$ such that ($\forall d_N = d_{N + n}$).
	Note: The lub of this chain is $d_N$.
\end{mydef}

\begin{lemma}
	If $(D, \LT)$ is a partial order with $D$ is finite, then it is a cpo.
\end{lemma}
\begin{proof}
	Let $(d_i)_{i \in \mathbb{N}}$ be a chain, then it is eventually constant, and thus has a lub. $(D, \LT)$ is therefore a cpo.
\end{proof}

Can we think of a function $f : \Omega \rightarrow \Omega$ that is monotone but not continuous.
Yes. Map everything to 0, but $\omega$ to $\omega$.

\begin{lemma}
	In a flat domain, all chains are eventually constant.
\end{lemma}

\begin{lemma}
	Als in $D$ all chains eventually constant then: if $f : D \rightarrow E$ is monotone then $f$ is continuous.
\end{lemma}
