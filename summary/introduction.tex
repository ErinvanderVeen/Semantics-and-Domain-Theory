\section{Introduction}
\subsection{Semantics}
There are three ways to define the semantics of programming languages:
\begin{description}
	\item[Operational] Meanings of program phrases defined in terms of the steps of computation they can take during program execution
	\item[Axiomatic] Meanings for program phrases defined indirectly via the axioms and rules of some logic of program properties
	\item[Denotation] Concerned with giving mathematical models of programming languages. Meanings for program phrases defined abstractly as elements of mathematical structure.
\end{description}

In this course will we only consider Structured Operational Semantics.
\begin{mydef}[Structured Operational Semantics]
	Defining the meaning of a program through the changes in the program state:
	\[
		<P,s> \Rightarrow s'
	\]
	where $P$ is the program, $s$ is the state before the execution of a statement and $s'$ is the state after the execution.
\end{mydef}

In Denotational semantics we define statements as mathematical functions.
\begin{mydef}[Denotational Semantics]
	Defining the meaning of a program through the partial functions $State \rightharpoondown State$.
	\[
		\llbracket P \rrbracket : State \rightharpoondown State
	\]
	where $State \rightharpoondown State$ is the set of partial state transformers.
\end{mydef}

\subsection{Example: If}
We can now define the denotational semantics of the following statement:
\[
	\IF B \THEN C \ELSE C'
\]
where $\llbracket C \rrbracket, \llbracket C' \rrbracket : State \rightharpoondown State$ and $\llbracket B \rrbracket : State \rightarrow \{true, false\}$ as:
\begin{mydef}[Denotational Semantics of \IF \THEN \ELSE]
	\[
		\IF B \THEN C \ELSE C' = \lambda s \in State.if(\llbracket B \rrbracket(s), \llbracket C \rrbracket(s), \llbracket C' \rrbracket(s))
	\]
	where
	\[
		if(b, c, c') =
		\begin{cases}
			x &\text{if } b = true\\
			x' &\text{if } b = false\\
		\end{cases}
	\]
\end{mydef}

\subsection{Composition}
We require composition in denotational semantics:
\begin{mydef}[Composition]
	\[
		\llbracket C;C' \rrbracket = \llbracket C' \rrbracket \circ \llbracket C \rrbracket
	\]
\end{mydef}

\subsection{Example: while as fixed point}
For the operational semantics, we can define the while-loop as an \IF statement, followed by a \WHILE statement.
If we try to do the same in denotational semantics, we have a problem:
\[
	\llbracket \WHILE B \DO C \rrbracket = \llbracket \IF B \THEN C; (\WHILE B \DO C) \ELSE \SKIP \rrbracket
\]
We use the denotation of \WHILE on the right-hand side of the very function we are trying to define.
The solution is to use fixed points:
\begin{mydef}[Fixed Point]
	Given a function $f$, the fixed point is a point $x$ in the domain of $f$, such that:
	\[
		f(x) = x
	\]
\end{mydef}

If we then define the following equation:
\begin{mydef}[While Fixed Point]
\[
	\llbracket \WHILE B \DO C \rrbracket = f_{\llbracket B \rrbracket, \llbracket C \rrbracket}(\llbracket \WHILE B \DO C \rrbracket)
\]
where
\[
	f_{b,c}(w) = \lambda s \in State. if(b(s), w(c(s)), s)
\]
\end{mydef}
Obviously, the denotation of \WHILE is the fixed point of the function $f$.
